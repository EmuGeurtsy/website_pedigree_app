﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Geurts.UIGod
{
    public class WarningMessage : Message
    {
        public Button _continueButton;
        public Button _cancelButton;
    }
}