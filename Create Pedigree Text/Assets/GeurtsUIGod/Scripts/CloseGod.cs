﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geurts.UIGod
{
    public class CloseGod : MonoBehaviour
    {
        [SerializeField] private GameObject[] _panelsToClose;

        public void CloseParentCanvas()
        {
            Transform parent = transform.parent;

            if (!IsParentPresent(parent))
            {
                return;
            }

            while (parent.GetComponent<Canvas>() == null)
            {
                parent = parent.parent;

                if (!IsParentPresent(parent))
                {
                    return;
                }
            }

            parent.gameObject.SetActive(false);
        }

        public void ClosePanels()
        {
            for (int index = 0; index < _panelsToClose.Length; index++)
            {
                _panelsToClose[index].SetActive(false);
            }
        }

        public void ClosePanel(GameObject panel)
        {
            if (panel == null)
            {
                Debug.LogError("No Panel Selected");
                return;
            }

            panel.SetActive(false);
        }

        private bool IsParentPresent(Transform parent)
        {
            if (parent == null)
            {
                Debug.LogError("No Parent Found");
                return false;
            }

            return true;
        }
    }
}
