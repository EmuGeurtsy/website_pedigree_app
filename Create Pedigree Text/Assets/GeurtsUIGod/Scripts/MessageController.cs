﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Geurts.UIGod
{
    public class MessageController : MonoBehaviour
    {
        [Header("Prefabs")]
        [SerializeField] private GameObject _warningMsgPrefab;
        [SerializeField] private GameObject _errorMsgPrefab;

        private bool _isMessageActive;
        private Message _currentMessage;

        public void ShowWarningMessage(string message, UnityAction cancelAction, UnityAction continueAction)
        {
            if (_isMessageActive)
                return;

            WarningMessage warningMsg = Instantiate(_warningMsgPrefab, transform).GetComponent<WarningMessage>();

            if(cancelAction != null)
                warningMsg._cancelButton.onClick.AddListener(cancelAction);
            
            warningMsg._cancelButton.onClick.AddListener(DestroyCurrentMessage);

            if(continueAction != null)
                warningMsg._continueButton.onClick.AddListener(continueAction);

            warningMsg._continueButton.onClick.AddListener(DestroyCurrentMessage);

            warningMsg.DisplayMessage(message);

            _isMessageActive = true;
            _currentMessage = warningMsg;
        }

        public void ShowErrorMessage(string message, UnityAction cancelAction)
        {
            if (_isMessageActive)
                return;

            ErrorMessage errorMessage = Instantiate(_errorMsgPrefab, transform).GetComponent<ErrorMessage>();

            if(cancelAction != null)
                errorMessage._cancelButton.onClick.AddListener(cancelAction);
            
            errorMessage._cancelButton.onClick.AddListener(DestroyCurrentMessage);

            errorMessage.DisplayMessage(message);

            _currentMessage = errorMessage;
            _isMessageActive = true;
        }

        public void DestroyCurrentMessage()
        {
            Destroy(_currentMessage.gameObject);

            _isMessageActive = false;
        }
    }
}
