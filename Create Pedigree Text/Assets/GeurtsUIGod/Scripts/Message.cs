﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Geurts.UIGod
{
    public class Message : MonoBehaviour
    {
        [SerializeField] private TMP_Text _messageText;

        public void DisplayMessage(string message)
        {
            _messageText.text = message;
        }
    }
}