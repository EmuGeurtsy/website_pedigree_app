﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geurts.UIGod
{
    public class OpenGod : MonoBehaviour
    {
        public GameObject[] _panelsToOpen;

        public void OpenPanels()
        {
            for (int index = 0; index < _panelsToOpen.Length; index++)
            {
                _panelsToOpen[index].SetActive(true);
            }
        }

        public void OpenPanel(GameObject panel)
        {
            if(panel == null)
            {
                Debug.LogError("No Panel Selected");
                return;
            }

            panel.SetActive(true);
        }
    }
}