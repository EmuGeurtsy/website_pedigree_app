﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class K9CardEditor : MonoBehaviour
{
    [Header("Input Fields")]
    [SerializeField] private TMP_InputField _nameInput;
    [SerializeField] private TMP_InputField _descriptionInput;
    [SerializeField] private TMP_InputField _k9Link;

    [Header("References")]
    [SerializeField] private K9CardCreator _k9CardCreator;
    [SerializeField] private K9CardLoader _k9CardLoader;

    private K9CardData _newCardData;

    private K9Card _selectedCard;

    public void EditCard(K9Card card)
    {
        _newCardData = card.LoadData();

        _selectedCard = card;

        DisplayCardData();
    }

    public void HideEditor()
    {
        gameObject.SetActive(false);
    }

    public void SaveChanges()
    {
        _newCardData._name = _nameInput.text;
        _newCardData._descripition = _descriptionInput.text;
        _newCardData._link = _k9Link.text;

        _selectedCard.SetData(_newCardData);

        if(_newCardData._ID == -1)
        {
            _k9CardLoader.CreateNewCard(_selectedCard.LoadData());
        }

        gameObject.SetActive(false);

    }

    public void DisplayCardData()
    {
        _nameInput.text = _newCardData._name;
        _descriptionInput.text = _newCardData._descripition;
        _k9Link.text = _newCardData._link;
    }

    public void DiscardChanges()
    {
        gameObject.SetActive(false);
    }

    public void DeleteCard()
    {
        _k9CardLoader.RemoveCardData(_selectedCard.LoadData());

        K9CardData newCardData = _k9CardCreator.CreateTemplateCardData();
        _selectedCard.SetData(newCardData);

        gameObject.SetActive(false);
    }
}
