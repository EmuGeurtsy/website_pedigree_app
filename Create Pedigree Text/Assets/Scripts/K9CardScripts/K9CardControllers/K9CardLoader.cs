﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Geurts.SaveGod;
using System.Linq;
using System;

[RequireComponent(typeof(K9CardCreator))]
public class K9CardLoader : MonoBehaviour
{
    public K9CardContainer _cardContainer;

    private K9CardCreator _k9CardCreator;

    private List<K9CardData> _currentDatas;

    private void Start()
    {
        _k9CardCreator = GetComponent<K9CardCreator>();

        LoadK9Cards();


        _currentDatas = new List<K9CardData>();
        _currentDatas = _cardContainer._cardDatas.ToList();
    }

    public void RemoveCardData(K9CardData cardData)
    {
        _currentDatas.Remove(cardData);

        SaveAllCardData();
    }

    public void CreateNewCard(K9CardData cardData)
    {
        cardData._ID = GetAvailableID();
        _currentDatas.Add(cardData);

        SaveAllCardData();
    }

    public void SaveCardData(K9CardData cardData)
    {
        for(int index = 0; index < _currentDatas.Count; index++)
        {
            if(_currentDatas[index]._ID == cardData._ID)
            {
                _currentDatas[index] = cardData;
            }
        }

        SaveAllCardData();
    }

    public void SaveAllCardData()
    {
        _cardContainer._cardDatas = _currentDatas.ToArray();

        SaveGod.GenericSave(_cardContainer);
    }

    private void LoadK9Cards()
    {
        if (SaveGod.CheckForSave(ref _cardContainer))
            SaveGod.GenericLoad(ref _cardContainer);
        else
            CreateTemplateContainer();
    }

    public bool DoesK9Exist(string name)
    {
        for(int index = 0; index < _cardContainer._cardDatas.Length; index++)
        {
            if(_cardContainer._cardDatas[index]._name == name)
            {
                return true;
            }
        }

        return false;
    }

    public bool DoesK9Exist(int id)
    {
        for (int index = 0; index < _cardContainer._cardDatas.Length; index++)
        {
            if (_cardContainer._cardDatas[index]._ID == id)
            {
                return true;
            }
        }

        return false;
    }

    public K9CardData GetK9CardData(int id)
    {
        if(DoesK9Exist(id))
        {
            for(int index = 0; index < _cardContainer._cardDatas.Length; index++)
            {
                if(_cardContainer._cardDatas[index]._ID == id)
                {
                    return _cardContainer._cardDatas[index];
                }
            }
        }

        return null;
    }

    public K9CardData GetK9CardData(string name)
    {
        if (DoesK9Exist(name))
        {
            for (int index = 0; index < _cardContainer._cardDatas.Length; index++)
            {
                if (_cardContainer._cardDatas[index]._name == name)
                {
                    return _cardContainer._cardDatas[index];
                }
            }
        }

        return null;
    }

    public int GetAvailableID()
    {
        int testID = 0;

        while(DoesK9Exist(testID))
        {
            testID++;
        }

        return testID;
    }

    public void CreateTemplateContainer()
    {
        K9CardContainer cardContainer = new K9CardContainer();

        K9CardData unknownCard = _k9CardCreator.CreateTemplateCardData();

        cardContainer._cardDatas = new K9CardData[1] { unknownCard };

        SaveGod.GenericSave(cardContainer);
    }
}
