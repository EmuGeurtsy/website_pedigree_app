﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class K9Card : MonoBehaviour
{
    [Header("Text Fields")]
    [SerializeField] private TMP_Text _nameField;
    [SerializeField] private TMP_Text _descriptionField;

    [Header("Buttons")]
    [SerializeField] public Button _editButton;
    [SerializeField] public Button _linkButton;

    private K9CardData _myCardData;

    public string GetLink
    {
        get { return _myCardData._link; }
        private set { _myCardData._link = value; }
    }

    public void SetData(K9CardData cardData)
    {
        _myCardData = cardData;
        
        if(_myCardData._link == "")
        {
            _linkButton.interactable = false;
        }
        else
        {
            _linkButton.interactable = true;
        }

        DisplayChanges();
    }

    public K9CardData LoadData()
    {
        return _myCardData;
    }


    public void DisplayChanges()
    {
        _nameField.text = _myCardData._name;
        _descriptionField.text = _myCardData._descripition;
    }

    public void DeleteCard()
    {

    }

    public void OpenLink()
    {

    }
}
