﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class K9CardData
{
    public int _ID;
    public string _name;
    public string _descripition;
    public string _link;

    public int _sireID;
    public int _damID;
}
