﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Geurts.SaveGod;

[System.Serializable]
public class K9CardContainer : ISaveType
{
    public string SaveFileName { get; set; } = "CardData";

    public K9CardData[] _cardDatas;
}
