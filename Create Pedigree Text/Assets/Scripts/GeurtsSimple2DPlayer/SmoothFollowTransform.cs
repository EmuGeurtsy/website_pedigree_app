﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geurts.Movement.Simple2DMove
{
    public class SmoothFollowTransform : MonoBehaviour
    {
        [SerializeField] private Transform _stalkedTransform;
        [SerializeField] private float _latency;
        [SerializeField] private float _justifableProximity;

        private void FixedUpdate()
        {
            Vector3 targetPosition = new Vector3(_stalkedTransform.position.x, _stalkedTransform.position.y, -10);

            float distance = Vector3.Magnitude(targetPosition - transform.position);

            if (distance >= _justifableProximity)
            {
                transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * _latency);
            }
        }
    }
}
