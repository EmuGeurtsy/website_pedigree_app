﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geurts.Movement.Simple2DMove
{
    public class SmoothMoveTransform : MonoBehaviour
    {
        [SerializeField] private float _speed;

        private void FixedUpdate()
        {
            float horizontalSpeed = Input.GetAxis("Horizontal") * Time.deltaTime * _speed;
            float verticalSpeed = Input.GetAxis("Vertical") * Time.deltaTime * _speed;

            transform.position += new Vector3(horizontalSpeed, verticalSpeed);
        }
    }
}

