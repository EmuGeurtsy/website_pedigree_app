﻿namespace Geurts.SaveGod
{
    public interface ISaveType
    {
        string SaveFileName { get; set; }
    }
}
