﻿using UnityEngine;
using System.Xml.Serialization;
using System.Text;
using System.IO;

/* Notes
 * Ensure all saveable data types have ISaveType inheritance.
 * Ensure all saveable data types have [System.Serializable] attribute.
 */


namespace Geurts.SaveGod
{
    public static class SaveGod
    {
        public static void GenericSave<T>(T saveObject) where T : ISaveType
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            string path = GetGenericPath(saveObject.SaveFileName);
            var encoding = Encoding.GetEncoding("UTF-8");

            StreamWriter stream = new StreamWriter(path, false, encoding);
            serializer.Serialize(stream, saveObject);
            stream.Close();
        }

        public static bool CheckForSave<T>(ref T saveObject) where T : class, ISaveType
        {
            string path = GetGenericPath(saveObject.SaveFileName);

            if(File.Exists(path))
            {
                return true;
            }

            return false;
        }

        public static void GenericLoad<T>(ref T saveObject) where T : class, ISaveType
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            string path = GetGenericPath(saveObject.SaveFileName);

            if (!File.Exists(path))
                return;

            MemoryStream stream = new MemoryStream(File.ReadAllBytes(path));

            if(stream != null)
            {
                saveObject = serializer.Deserialize(stream) as T;
            }

            stream.Close();
        }

        public static void RemoveGenericSave(string fileName)
        {
            File.Delete(GetGenericPath(fileName));
        }

        private static string GetGenericPath(string fileName)
        {
            return (Application.persistentDataPath  + $"\\{fileName}.xml");
        }
    }
}

//namespace K9Profiler.Tools
//{
//    public class DateHandler
//}