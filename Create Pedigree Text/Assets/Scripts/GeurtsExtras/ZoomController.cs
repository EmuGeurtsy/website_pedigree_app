﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomController : MonoBehaviour
{
    [SerializeField] private float _scrollSpeed;
    [SerializeField] private int _scrollSpeedMultiplier;

    [SerializeField] private int minZoom, maxZoom;

    private void OnEnable()
    {
        if (Camera.main.orthographic == false)
        {
            Debug.LogError("ZoomController will not function because camera is not in orthographic mode.");
            Destroy(this);
        }

        Camera.main.orthographicSize = 500;
    }

    private void FixedUpdate()
    {
        Scroll();
    }

    private void Scroll()
    {
        float scrollSpeed = Input.GetAxis("MouseScrollWheel") * Time.deltaTime * _scrollSpeed;
        scrollSpeed *= _scrollSpeedMultiplier;

        Camera.main.orthographicSize += scrollSpeed;

        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, minZoom, maxZoom);
    }
}
