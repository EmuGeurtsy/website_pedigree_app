﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentationController : MonoBehaviour
{
    public static bool _isEditing = false;
    [SerializeField] private bool _isEditingUnity;

    [Header("Presentation Parents")]
    [SerializeField] private GameObject editParent;
    [SerializeField] private GameObject presentParent;

    public void Start()
    {
        _isEditing = _isEditingUnity;

        if(_isEditing == true)
        {
            StartEditMode();
        }
        else
        {
            StartPresentationMode();
        }
    }

    private void StartEditMode()
    {
        editParent.SetActive(true);

        Destroy(presentParent);
    }

    private void StartPresentationMode()
    {
        presentParent.SetActive(true);

        Destroy(editParent);
    }
}
