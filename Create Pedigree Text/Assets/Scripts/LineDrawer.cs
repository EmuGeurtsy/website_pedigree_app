﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    [SerializeField] private float _zOffset;

    [Header("Colours")]
    [SerializeField] private Gradient _colourGradient;

    private List<LineRenderer> _activeLines;

    private void Start()
    {
        _activeLines = new List<LineRenderer>();
    }

    public void DeactivateLine(int index)
    {
        _activeLines[index].enabled = false;
    }

    public void DrawVecticalCornerLine(Vector2 firstPoint, Vector2 finalPoint, out int lineIndex)
    {
        LineRenderer line = new LineRenderer();

        Vector3 point1 = new Vector3(firstPoint.x, firstPoint.y, _zOffset);
        Vector3 point2 = new Vector3(firstPoint.x, finalPoint.y, _zOffset);
        Vector3 point3 = new Vector3(finalPoint.x, finalPoint.y, _zOffset);

        line.colorGradient = _colourGradient;

        line.SetPositions(new Vector3[] { point1, point2, point3 } );

        int freeIndex = -1;

        if (FindFreeIndex(out freeIndex))
        {
            _activeLines[freeIndex] = line;
            lineIndex = freeIndex;
        }
        else
        {
            _activeLines.Add(line);
            lineIndex = _activeLines.Count - 1;
        }
    }

    public void DrawDirectLine(Vector2 firstPoint, Vector2 finalPoint, out int lineIndex)
    {
        LineRenderer line = new LineRenderer();

        Vector3 point1 = new Vector3(firstPoint.x, firstPoint.y, _zOffset);
        Vector3 point2 = new Vector3(finalPoint.x, finalPoint.y, _zOffset);

        line.colorGradient = _colourGradient;

        line.SetPositions( new Vector3[]{ point1, point2 } );

        int freeIndex = -1;

        if (FindFreeIndex(out freeIndex))
        {
            _activeLines[freeIndex] = line;
            lineIndex = freeIndex;
        }
        else
        {
            _activeLines.Add(line);
            lineIndex = _activeLines.Count - 1;
        }
    }

    private bool FindFreeIndex(out int freeIndex)
    {
        for(int index = 0; index < _activeLines.Count; index++)
        {
            if(_activeLines[index].enabled != enabled)
            {
                freeIndex = index;
                return true;
            }
        }

        freeIndex = -1;

        return false;
    }
}
