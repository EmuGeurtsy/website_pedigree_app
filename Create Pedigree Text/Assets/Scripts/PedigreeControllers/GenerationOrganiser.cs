﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerationOrganiser : MonoBehaviour
{
    [HideInInspector] public int _generationNumber;

    [Header("Card Dimensions")]
    [SerializeField] private Vector2 _cardDimensions;
    [SerializeField] private float _coupleHeight;

    [Header("Adjustments")]
    [SerializeField] private float _coupleHeightAdjustment;
    [SerializeField] private float _cardHeightAdjustment;
    [SerializeField] private float _xPosAdjustment;

    private K9CardCreator _k9CardCreator;

    private RectTransform _myRectTransform;

    private void OnEnable()
    {
        _myRectTransform = GetComponent<RectTransform>();
    }

    public void PopulateGeneration(List<K9CardData> generation, K9CardCreator creatorInstance)
    {
        _k9CardCreator = creatorInstance;

        _k9CardCreator.CreateCards(generation, transform);
    }

    public void CorrectGenerationPosition(int generationNumber)
    {
        float newXPos = _cardDimensions.x * generationNumber;

        if(generationNumber != 1)
            newXPos += _xPosAdjustment;


        _myRectTransform.position = new Vector3(newXPos, 0);
    }

    public void AdjustForNewK9()
    {
        float newHeight = _cardDimensions.y + _cardHeightAdjustment + _myRectTransform.sizeDelta.y;
        _myRectTransform.sizeDelta.Set(_myRectTransform.sizeDelta.x, newHeight);
    }

    public void AdjustForNewCouple()
    {
        float newHeight = _coupleHeight + _coupleHeightAdjustment + _myRectTransform.sizeDelta.y;
        _myRectTransform.sizeDelta.Set(_myRectTransform.sizeDelta.x, newHeight);
    }
}