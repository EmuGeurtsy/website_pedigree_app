﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using Geurts.UIGod;
using UnityEngine.Events;
using System;
using UnityEditor;

[RequireComponent(typeof(PedigreeManager))]
[RequireComponent(typeof(K9CardLoader))]
public class PedigreeLoader : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private GameObject _generationPrefab;

    [Header("Other References")]
    [SerializeField] private GameObject _pedigreeGameObject;
    [SerializeField] private GameObject[] _loadMenuGameObjects;
    [SerializeField] private MessageController _messageController;

    private string _selectedPrimaryK9Name;

    private PedigreeManager _pedigreeManager;
    private K9CardLoader _k9CardLoader;


    private void Start()
    {
        _pedigreeManager = GetComponent<PedigreeManager>();
        _k9CardLoader = GetComponent<K9CardLoader>();
    }

    public void ChangeName(string name)
    {
        _selectedPrimaryK9Name = name;
    }

    public void ConfirmNewPedigree()
    {
        _messageController.ShowWarningMessage(
            "Creating a new pedigree will COMPLETELY OVERWRITE any existing data. Do you want to continue?" 
            , null, CreateNewPedigree);
    }

    private void CreateNewPedigree()
    {
        _k9CardLoader.CreateTemplateContainer();

        LoadPedigree(_k9CardLoader.GetK9CardData(-1)._name);
    }

    public void LoadPedigree(string name)
    {
        List<List<K9CardData>> generations = new List<List<K9CardData>>();

        if(name == "")
            name = _selectedPrimaryK9Name;


        if(_k9CardLoader.DoesK9Exist(name))
        {
            K9CardData primaryCard = _k9CardLoader.GetK9CardData(name);
            generations = FindGenerations(primaryCard);
        }
        else
        {
            _messageController.ShowErrorMessage( "Could not find K9 name in saved data.", null );
            return;
        }

        _pedigreeManager.CreatePedigree(generations);

        for (int index = 0; index < _loadMenuGameObjects.Length; index++)
        {
            _loadMenuGameObjects[index].SetActive(false);
        }
    }

    public List<List<K9CardData>> FindGenerations(K9CardData cardData)
    {
        List<List<K9CardData>> generations = new List<List<K9CardData>>();
        List<K9CardData> currentGeneration = new List<K9CardData>();

        currentGeneration.Add(cardData);
        generations.Add(currentGeneration);

        while(CheckCardsHasCouple(currentGeneration))
        {
            List<K9CardData> newGeneration = new List<K9CardData>();

            newGeneration = FindRelations(currentGeneration);
            generations.Add(newGeneration);

            currentGeneration = newGeneration;
        }

        Debug.Log("Gen 1: " + generations[0][0]._name);

        return generations;
    }

    public bool CheckCardsHasCouple(List<K9CardData> cardDatas)
    {
        for(int index = 0; index < cardDatas.Count; index++)
        {
            if(IsCouplePresent(cardDatas[index]))
            {
                return true;
            }
        }

        return false;
    }

    public List<K9CardData> FindRelations(List<K9CardData> cardDatas)
    {
        List<K9CardData> generation = new List<K9CardData>();

        for(int index = 0; index < cardDatas.Count; index++)
        {
            int damID = cardDatas[index]._damID;
            int sireID = cardDatas[index]._sireID;

            generation.Add(_k9CardLoader.GetK9CardData(damID));
            generation.Add(_k9CardLoader.GetK9CardData(sireID));
        }

        return generation;
    }

    public bool IsCouplePresent(K9CardData cardData)
    {
        if (cardData._damID == -1 && cardData._sireID == -1)
        {
            return false;
        }

        return true;
    }
}
