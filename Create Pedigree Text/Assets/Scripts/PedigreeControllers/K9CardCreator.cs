﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class K9CardCreator : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private GameObject _k9CardPrefab;
    [SerializeField] private GameObject _couplePrefab;

    [Header("Card Editor")]
    [SerializeField] private K9CardEditor _k9CardEditor;

    public K9CardData CreateTemplateCardData()
    {
        K9CardData cardTemplate = new K9CardData()
        {
            _name = "Unknown",
            _descripition = "This is a short example description. A K9 Description will be placed here",
            _link = "",
            _ID = -1,
            _damID = -1,
            _sireID = -1
        };

        return cardTemplate;
    }

    public void CreateCards(List<K9CardData> cardData, Transform parentTransform)
    {
        int pair = 0;

        if(cardData.Count == 1)
        {
            CreateK9Card(cardData[0], parentTransform);
            return;
        }


        for(int index = 0; pair < cardData.Count; index++)
        {
            pair++;

            GameObject damCard = CreateK9Card(cardData[index], parentTransform);
            GameObject sireCard = CreateK9Card(cardData[pair], parentTransform);

            CreateCouple(sireCard, damCard, parentTransform);
        }
    }

    public void CreateCouple(GameObject sireCard, GameObject damCard, Transform parentTransform)
    {
        Transform coupleTransform = Instantiate(_couplePrefab, parentTransform).transform;

        damCard.transform.SetParent(coupleTransform);
        sireCard.transform.SetParent(coupleTransform);
    }

    public GameObject CreateK9Card(K9CardData cardData, Transform parentTransform)
    {
        GameObject cardObject = Instantiate(_k9CardPrefab, parentTransform);
        K9Card card = cardObject.GetComponent<K9Card>();

        card.SetData(cardData);
        LinkButtons(card);

        return cardObject;
    }

    public void LinkButtons(K9Card card)
    {
        if(PresentationController._isEditing == false)
        {
            Destroy(card._editButton);
        }
        else
        {
            card._editButton.onClick.AddListener(delegate { EditK9Card(card); });
        }
 
        card._linkButton.onClick.AddListener(delegate { GoToLink(card.GetLink); });
    }

    public void EditK9Card(K9Card card)
    {
        OpenCardEditor();

        _k9CardEditor.EditCard(card);
    }

    public void GoToLink(string link)
    {
        Application.OpenURL(link);
    }

    public void OpenCardEditor()
    {
        _k9CardEditor.gameObject.SetActive(true);
    }

    public void DedicateGeneration(K9Card k9Card)
    {

    }
}
