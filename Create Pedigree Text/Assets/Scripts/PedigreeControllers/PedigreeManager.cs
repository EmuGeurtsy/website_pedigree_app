﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(K9CardCreator))]
public class PedigreeManager : MonoBehaviour
{
    [SerializeField] private GameObject _playerDog;

    [Header("Prefabs")]
    [SerializeField] private GameObject _generationPrefab;

    [Header("Other Variables")]
    [SerializeField] private Transform _pedigreeTransform;

    private List<GenerationOrganiser> _generationOrganisers;
    private int _activeGenerationCount = 0;

    private K9CardCreator _k9CardCreator;

    private void OnEnable()
    {
        _generationOrganisers = new List<GenerationOrganiser>();
    }

    private void Awake()
    {
        _k9CardCreator = GetComponent<K9CardCreator>();
    }

    public void MakeDogLastChild()
    {
        _playerDog.transform.SetAsLastSibling();
    }

    public void CreatePedigree(List<List<K9CardData>> generations)
    {
        for (int index = 0; index < generations.Count; index++)
        {
            CreateGeneration(generations[index]);
        }
    }

    private void CreateGeneration(List<K9CardData> generation)
    {
        GameObject newGameObject = Instantiate(_generationPrefab, transform);
        GenerationOrganiser generationOrganiser = newGameObject.GetComponent<GenerationOrganiser>();

        generationOrganiser.CorrectGenerationPosition(_activeGenerationCount);
        generationOrganiser.PopulateGeneration(generation, _k9CardCreator);

        _generationOrganisers.Add(generationOrganiser);

        _activeGenerationCount++;

        MakeDogLastChild();
    }
}
